package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.product.vo.AttrAttrGroupRelationVo;
import com.atguigu.gulimall.product.vo.AttrRespVo;
import com.atguigu.gulimall.product.vo.AttrVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.AttrEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author wangzhijun
 * @email 1025210489@qq.com
 * @date 2021-01-29 13:39:20
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVo attr);

    PageUtils queryBaseListPage(Map<String, Object> params, Long catelogId, String attrType);

    AttrRespVo getAttrVoById(Long attrId);

    void updateAttr(AttrVo attr);

    List<Long> selectSearchAttrIds(List<Long> attrIds);

    List<AttrEntity> getAttRrelation(Long attrGroupId);

    void deleteRelation(List<AttrAttrGroupRelationVo> attrGroupRelationVo);

    PageUtils noattrRelation(Long attrgroupId, Map<String, Object> params);
}

