package com.atguigu.gulimall.product.entity;

import com.atguigu.common.utils.validate.AddGroup;
import com.atguigu.common.utils.validate.UpdateGroup;
import com.atguigu.common.utils.validate.UpdateStatusGroup;
import com.atguigu.common.utils.validate.annotation.ListValue;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * 品牌
 *
 * @author wangzhijun
 * @email 1025210489@qq.com
 * @date 2021-01-29 13:39:19
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 品牌id
     */
    @NotNull(message = "编辑操作时品牌id不能为空", groups = {UpdateGroup.class})
    @Null(message = "添加操作品牌id必须为空", groups = {AddGroup.class})
    @TableId
    private Long brandId;
    /**
     * 品牌名
     */
    @NotBlank(message = "品牌名不能为空！", groups = {AddGroup.class, UpdateGroup.class})
    private String name;
    /**
     * 品牌logo地址
     */
    @NotBlank(groups = {AddGroup.class})
    @URL(message = "logo必须是一个合法的url", groups = {AddGroup.class, UpdateGroup.class})
    private String logo;
    /**
     * 介绍
     */
    private String descript;
    /**
     * 显示状态[0-不显示；1-显示]
     */
    @NotNull(message = "显示状态不能为空", groups = {AddGroup.class, UpdateStatusGroup.class})
    @ListValue(vals = {0, 1}, groups = {AddGroup.class, UpdateStatusGroup.class})
    private Integer showStatus;
    /**
     * 检索首字母
     */
    @NotEmpty(message = "首字母必须填写！", groups = {AddGroup.class})
    @Pattern(regexp = "^[A-Za-z]$", message = "首字母必须以A-Z或者a-z字母开头！", groups = {AddGroup.class, UpdateGroup.class})
    private String firstLetter;
    /**
     * 排序
     */
    @NotNull(message = "排序必须填写！", groups = {AddGroup.class})
    @Min(value = 0, message = "排序必须是一个大于0的整数！", groups = {AddGroup.class, UpdateGroup.class})
    private Integer sort;

}
