package com.atguigu.gulimall.product.service.impl;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Transactional
@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<>()
        );
        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        List<CategoryEntity> list = baseMapper.selectList(null);
        List<CategoryEntity> parentList = list.stream().filter(o -> o.getParentCid() == 0).map(o -> {
            o.setChildren(getChildrens(o, list));
            return o;
        }).sorted((o1, o2) -> {
            return (Objects.isNull(o1.getSort()) ? 0 : o1.getSort()) - (Objects.isNull(o2.getSort()) ? 0 : o2.getSort());
        }).collect(Collectors.toList());
        return parentList;
    }

    @Override
    public void removeMenusByIds(List<Long> asList) {
        baseMapper.deleteBatchIds(asList);
    }

    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> path = new ArrayList<>();
        List<Long> catelogPath = this.findFullCatelogPath(catelogId, path);
        Collections.reverse(catelogPath);
        return catelogPath.toArray(new Long[catelogPath.size()]);
    }

    @Override
    public void updateDetails(CategoryEntity entity) {
        this.updateById(entity);
        if (!StringUtils.isEmpty(entity.getName())) {
            // 修改关系表冗余字段数据
            categoryBrandRelationService.updateCategory(entity.getCatId(), entity.getName());

            // TODO
        }
    }

    private List<Long> findFullCatelogPath(Long catelogId, List<Long> path) {
        path.add(catelogId);
        CategoryEntity category = this.getById(catelogId);
        if (!Objects.equals(category.getParentCid(), 0l)) {
            findFullCatelogPath(category.getParentCid(), path);
        }
        return path;
    }

    /**
     * 递归实现类目数据完成父子类目展示
     *
     * @param categoryEntity
     * @param list
     * @return
     */
    private List<CategoryEntity> getChildrens(CategoryEntity categoryEntity, List<CategoryEntity> list) {
        List<CategoryEntity> children = list.stream().filter(o -> o.getParentCid() == categoryEntity.getCatId()).map(o -> {
            o.setChildren(getChildrens(o, list));
            return o;
        }).sorted((o1, o2) -> {
            return (Objects.isNull(o1.getSort()) ? 0 : o1.getSort()) - (Objects.isNull(o2.getSort()) ? 0 : o2.getSort());
        }).collect(Collectors.toList());
        return children;
    }


}