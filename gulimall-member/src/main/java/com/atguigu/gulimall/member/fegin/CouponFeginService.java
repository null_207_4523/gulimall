package com.atguigu.gulimall.member.fegin;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("gulimall-coupon")//服务名
public interface CouponFeginService {

    @GetMapping("/coupon/coupon/coupon/list")
    public R couponlist();
}
