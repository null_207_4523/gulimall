package com.atguigu.gulimall.auth.component;

import com.alibaba.fastjson.JSON;
import com.atguigu.common.utils.HttpUtils;
import com.atguigu.gulimall.auth.vo.SocialUser;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "oauth.weibo")
public class WeiboComponent {

    private String host;
    private String path;
    private String clientId;
    private String clientSecret;
    private String grantType;
    private String redirectUri;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public SocialUser getSocialUser(String code) {
        String method = "POST";
        Map<String, String> headers = new HashMap<>();
        Map<String, String> querys = new HashMap<String, String>();
        // 根据code换取 Access Token
        Map<String, String> bodys = new HashMap<>();
        bodys.put("client_id", clientId);
        bodys.put("client_secret", clientSecret);
        bodys.put("grant_type", grantType);
        bodys.put("redirect_uri", redirectUri);
        bodys.put("code", code);
        try {
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            if (response.getStatusLine().getStatusCode() == 200) {
                // 获取响应体： Access Token
                String json = EntityUtils.toString(response.getEntity());
                SocialUser socialUser = JSON.parseObject(json, SocialUser.class);
                return socialUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
