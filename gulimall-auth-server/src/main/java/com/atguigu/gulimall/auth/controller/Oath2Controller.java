package com.atguigu.gulimall.auth.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.auth.component.WeiboComponent;
import com.atguigu.gulimall.auth.feign.MemberFeignService;
import com.atguigu.gulimall.auth.vo.MemberRsepVo;
import com.atguigu.gulimall.auth.vo.SocialUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * 第三方应用登录
 */
@Slf4j
@Controller
@RequestMapping("/oauth2.0")
public class Oath2Controller {

    @Autowired
    private MemberFeignService memberFeignService;
    @Autowired
    private WeiboComponent weiboComponent;

    @GetMapping("/weibo/success") // Oath2Controller
    public String weiBo(@RequestParam("code") String code, HttpSession session) throws Exception {
        SocialUser socialUser = weiboComponent.getSocialUser(code);
        if(Objects.nonNull(socialUser)){
            // 相当于我们知道了当前是那个用户
            // 1.如果用户是第一次进来 自动注册进来(为当前社交用户生成一个会员信息 以后这个账户就会关联这个账号)
            R login = memberFeignService.oauth2Login(socialUser);
            if (login.getCode() == 0) {
                MemberRsepVo rsepVo = login.getData(new TypeReference<MemberRsepVo>() {
                });
                log.info("\n欢迎 [" + rsepVo.getUsername() + "] 使用社交账号登录");
                // 第一次使用session 命令浏览器保存这个用户信息 JESSIONSEID 每次只要访问这个网站就会带上这个cookie
                // 在发卡的时候扩大session作用域 (指定域名为父域名)
                // TODO 1.默认发的当前域的session (需要解决子域session共享问题)
                // TODO 2.使用JSON的方式序列化到redis
                //				new Cookie("JSESSIONID","").setDomain("gulimall.com");
//                session.setAttribute(AuthServerConstant.LOGIN_USER, rsepVo);
                // 登录成功 跳回首页
                return "redirect:http://gulimall.com";
            } else {
                return "redirect:http://auth.gulimall.com/login.html";
            }
        } else {
            return "redirect:http://auth.gulimall.com/login.html";
        }

    }

}
