package com.atguigu.common.exception;

public enum BizCodeEnum {

    UNKNOW_EXCEPTION(10000, "系统未知异常"),
    VALID_EXCEPTION(10001, "系统格式校验失败"),
    PRODUCT_UP_EXCEPTION(11000, "系统未知异常"),
    SMS_SEND_CODE_EXCEPTION(12000,"短信发送异常"),
    USER_EXIST_EXCEPTION(12001,"用户已存在异常"),
    PHONE_EXIST_EXCEPTION(12002,"手机号已存在异常"),
    LOGINACCT_PASSWORD_EXCEPTION(12003,"用户名或密码错误"),
    SMS_CODE_EXCEPTION(12100,"短信发送频繁异常");

    private int code;
    private String msg;

    BizCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
