package com.atguigu.common.constant;

public class ProductConstant {

    public enum AttrType {
        ATTR_TYPE_SALE(0, "销售属性"),
        ATTR_TYPE_BASE(1, "基本属性");
        private Integer code;
        private String name;

        AttrType(Integer code, String name) {
            this.code = code;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    public enum StatusType {
        SPU_NEW(0, "新建商品"),
        SPU_UP(1, "商品上架"),
        SPU_DOWN(2, "商品下架");
        private Integer code;
        private String name;

        StatusType(Integer code, String name) {
            this.code = code;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

}
